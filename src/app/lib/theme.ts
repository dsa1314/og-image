interface ThemeSetting {
  backgroundColor: string;
  subBackgroundColor: string;
  signatureColor: string;
  textColor: string;
}

export const dark:　ThemeSetting = {
  backgroundColor: '#15202b',
  subBackgroundColor: '#213345',
  signatureColor: '#1a91da',
  textColor: '#ffffff'
}

export const light: ThemeSetting = {
  backgroundColor: '#ffffff',
  subBackgroundColor: '#e0e9e9',
  signatureColor: '#1a91da',
  textColor: '#15202b'
}